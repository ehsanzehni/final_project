<?php


namespace App\Controller;



use App\Entity\Product;
use App\Form\Type\ProductFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;



class ProductHandle extends AbstractController
{
    /**
     * @Route( "/admin/product/create" , name ="create_product_by_admin")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */

    public function createProductByForm(Request $request, EntityManagerInterface $em)
    {
        $createProduct = new Product();
        return $this->handleProductByForm($request, $createProduct, $em, 0);
    }

    /**
     * @Route( "/admin/product/edit/{id}" , name ="edit_product_by_admin", requirements = {"id" = "\d+"} )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Product $product
     * @return Response
     */

    public function editProductByForm(Request $request, EntityManagerInterface $em, Product $product)
    {
        $path = $this->getParameter('image_directory').'/'.$product->getImage();
        $fileNew = new Filesystem();
        $fileNew->remove(array($path));
        $product->setImage(null);
        return $this->handleProductByForm($request, $product, $em, 1);
    }

    /**
     * @Route( "/admin/product/remove/{id}" , name ="remove_product_by_admin", requirements = {"id" = "\d+"} )
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param Product $product
     * @return Response
     */

    public function removeProductByForm(Request $request, EntityManagerInterface $em, Product $product)
    {
        $em->remove($product);
        $em->flush();
        $products = $this->findAllProducts();
        return $this->render('home.html.twig', ['products' => $products]);
    }

    /**
     * @param Request $request
     * @param Product $product
     * @param EntityManagerInterface $em
     * @return Response
     */
    private function handleProductByForm(Request $request, Product $product, EntityManagerInterface $em, Int $status): Response
    {



        $form = $this->createForm(ProductFormType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid()) {


            $file = $form->get('image')->getData();
            if ($file) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                try {
                    $file->move(
                        $this->getParameter('image_directory'),
                        $fileName
                    );
                } catch (FileException $e) {

                }
                $product->setImage($fileName);
                $em->persist($product);
                $em->flush();
                $products = $this->findAllProducts();
                return $this->render('home.html.twig', ['products' => $products]);
            }
            }
            if ($status == 0) {
                return $this->render('Product/create_product.html.twig', [
                    'form' => $form->createView()
                ]);
            } elseif ($status == 1) {
                return $this->render('Product/edit_product.html.twig', [
                    'form' => $form->createView()
                ]);
            }
        }
    public function findAllProducts()
    {
        $em = $this->getDoctrine()->getManager();
        $postRep = $em->getRepository(Product::class);
        return $postRep->findAll();
    }
    }

