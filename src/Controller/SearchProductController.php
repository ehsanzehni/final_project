<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\Type\ProductFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SearchProductController extends AbstractController
{
    /**
     * @Route("/search/handle", name="handleSearch")
     * @param Request $request
     * @param EntityManagerInterface $em
     */
    public function index(Request $request, EntityManagerInterface $em)
    {


        $form = $request->request->get('form');
            $search = $form['search'];


echo $search;

        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findBy(array('name'=>$search));
        print_r($products);

        return $this->render('home.html.twig', ['products' => $products]);

    }

    public function searchBar()
    {
        $form = $this->createFormBuilder(null)
            ->add('search',TextType::class)
            ->getForm();

        return $this->render('Search/searchBar.html.twig',[
            'form'=> $form->createView()
        ]);
    }
}
