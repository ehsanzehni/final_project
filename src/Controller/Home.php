<?php


namespace App\Controller;



use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Home extends AbstractController
{

    /**
     * @Route("/home/show/product" , name = "show_product")
     */
    public function showProducts()
    {
        $products = $this->findAllProducts();
        return $this->render('home.html.twig', ['products' => $products]);
    }

    public function findAllProducts()
    {
        $em = $this->getDoctrine()->getManager();
        $postRep = $em->getRepository(Product::class);
        return $postRep->findAll();
    }
}