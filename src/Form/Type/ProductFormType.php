<?php


namespace App\Form\Type;



use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name",TextType::class,['label'=>'نام محصول *'])
            ->add("price",TextType::class,['label'=>' قیمت محصول (تومان) *'])
            ->add('Description',TextareaType::class,['label'=>'توضیح محصول'])
            ->add("image",FileType::class,array('label'=>'اضافه نمودن تصویر محصول'))
            ->add("submit",SubmitType::class,['label' => 'اعمال تغییرات']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['data_class' => Product::class]);
    }

}